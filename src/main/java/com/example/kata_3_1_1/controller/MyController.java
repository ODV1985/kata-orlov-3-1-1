package com.example.kata_3_1_1.controller;

import com.example.kata_3_1_1.entity.MyUser;
import com.example.kata_3_1_1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MyController {

    private final UserService userService;

    @Autowired
    public MyController(UserService userService) {
        this.userService = userService;
    }

    // Метод для главной страницы, сразу при помощи View отображаю весь список из БД
    @GetMapping("/index")
    public String indexPage(Model model) {
        model.addAttribute("empAll", userService.getAllEmployee());
        return "info";
    }

    // при обращении к /formUserAdd/new срабатывает данный метод, создаю пустую сущность и при помощи
    // Класса Model помещаю туда сущность с дефолтными значениями. id Model user.
    // HTML форма для добавления новой записи
    @GetMapping("/formUserAdd/new")
    public String formUserAdd(Model model) {
        MyUser myUser = new MyUser();
        model.addAttribute("user", myUser);
        return "formUserAdd";
    }

    // HTML форма для редактирования уже существующей записи
    @GetMapping("/formUserEdit/edit")
    public String formUserEdit(@RequestParam("updateId") Integer id, Model model) {
        MyUser userFind = userService.findUser(id);
        model.addAttribute("user", userFind);
        return "formUserEdit";
    }

    //Добавляю в форму Вьюшки с дефолтными значениями (новые значения), создаю новый объект!
    //Возвращяюсь на главную страницу
    @PostMapping("/addUser")
    public String addUser(@ModelAttribute("user") MyUser myUser) {
        userService.add(myUser);
        return "redirect:/index";
    }

    // Изменяю
    @PatchMapping("/update")
    public String updateEmployee(@ModelAttribute("user") MyUser myUser) {
        userService.updateUser(myUser);
        return "redirect:/index";
    }

    // Удаляю
    @DeleteMapping("/delete")
    public String deleteEmployee(@RequestParam("deleteId") Integer id) {
        userService.deleteUser(id);
        return "redirect:/index";
    }

}
