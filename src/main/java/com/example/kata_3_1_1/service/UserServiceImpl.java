package com.example.kata_3_1_1.service;

import com.example.kata_3_1_1.dao.UserDao;
import com.example.kata_3_1_1.entity.MyUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public void add(MyUser myUser) {
        userDao.add(myUser);
    }

    @Override
    public List<MyUser> getAllEmployee() {
        return userDao.getAllEmployee();
    }

    @Override
    @Transactional
    public void deleteUser(Integer id) {
        userDao.deleteUser(id);
    }

    @Override
    public MyUser findUser(Integer id) {
        return userDao.findUser(id);
    }

    @Override
    @Transactional
    public void updateUser(MyUser myUser) {
        userDao.updateUser(myUser);
    }

}
