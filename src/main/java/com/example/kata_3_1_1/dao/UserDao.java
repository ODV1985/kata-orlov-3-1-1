package com.example.kata_3_1_1.dao;

import com.example.kata_3_1_1.entity.MyUser;

import java.util.List;

public interface UserDao {
    void add(MyUser myUser);

    List<MyUser> getAllEmployee();

    void deleteUser(Integer id);

    MyUser findUser(Integer id);

    void updateUser(MyUser myUser);
}
